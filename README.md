# Voice-Enabled Calculator

A simple, user-friendly calculator with voice input functionality built using HTML, CSS, and JavaScript. The calculator allows users to input calculations either via buttons or voice commands using the Web Speech API.

## Features

- Basic arithmetic operations (addition, subtraction, multiplication, division).
- Clear (`AC`) and delete (`DE`) functions.
- Voice input for hands-free calculations.
- Responsive design.


## Screenshots

   ![image](https://github.com/manasvi0109/Calculator-using-voice/assets/171707742/c0a72c2c-2d6d-43a4-8e49-7837ad214edd)
   ![image](https://github.com/manasvi0109/Calculator-using-voice/assets/171707742/37d7cac1-fcf3-4382-a663-4eee611ac52c)
![image](https://github.com/manasvi0109/Calculator-using-voice/assets/171707742/a1a25c87-536f-4277-97df-61de6fe3c11b)



## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

Make sure you have a web browser that supports the Web Speech API (e.g., Google Chrome).

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/your-username/calculator-using-voice.git
